platform: linux
image_resource:
  type: docker-image 
  source: {repository: alpine}

inputs:
  - name: source-code

run:
  path: sh
  args:
  - -exc
  - |
    apk update && apk add curl --no-cache && curl -OL https://github.com/Tufin/oasdiff/releases/download/v1.6.4/oasdiff_1.6.4_linux_amd64.apk && apk add oasdiff_1.6.4_linux_amd64.apk --allow-untrusted
    oasdiff breaking source-code/${PREV} source-code/${NEW}
    oasdiff changelog source-code/${PREV} source-code/${NEW}