# If you make any changes to this file, ensure you make any equivalent changes to test-detect-change-executions.tpl, as well as minor-or-patch.tpl, if applicable

platform: linux 
image_resource:
  type: docker-image 
  source: {repository: alpine}

inputs:
  - name: source-code 

run:
  path: sh 
  args:
  - -exc 
  - |
    apk update && apk add curl --no-cache && curl -OL https://github.com/Tufin/oasdiff/releases/download/v1.6.4/oasdiff_1.6.4_linux_amd64.apk && apk add oasdiff_1.6.4_linux_amd64.apk --allow-untrusted
    test -n "$(oasdiff breaking source-code/${PREV} source-code/${NEW})" && echo "Breaking changes found between old swagger file and new one. Exiting with error" && exit 1 || echo "No breaking change found"