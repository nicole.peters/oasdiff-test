# FLAWS WITH DESIGN:
# I have two nearly identical files & haven't abstracted anything out. may or may not be able to
# - Any changes must be made in both..... :/
# --- CHECK, kind of
# --- i've done as much as i can, i think. tried commenting out the try & on-failure stuff, but without reindenting it doesn't work
# Swagger file paths must be changed in two places each time they're changed. Hopefully won't matter when I'm done messing with them?
# --- CHECK!
# Breaking-change-detecting task that detects breaking changes on *failure* is a confusing design. But like it makes sense when you know its because it's trying to stay as close to the other file as possible
# --- not check. maybe ignoring this
# --- with stuff abstracted out it's easier to read and therefore probably less of a problem
# --- plus this should only be run by somebody who just ran detect-change (presumably? if we're doing breaking changes on purpose then maybe not, idk...), so like, they should be familiar with it
# It is impossible for this task to fail. Either users need to be very careful (they should only run this if they've already run detect-change anyway) 
#   or this needs some sort of test task that can fail (if, eg, something goes horribly wrong with alpine or something)
# --- CHECK!

# If you make any changes to this file, ensure you make any equivalent changes to detect-change.tpl

{{ $group := .group }}
{{ $prev_swagger_file := .prev_swagger_file }}
{{ $new_swagger_file := .new_swagger_file }}

groups:
- name: {{ $group }}
  jobs:
    - "detect-change-allow-breaking-changes"

jobs:
- name: "detect-change-allow-breaking-changes"
  serial: true
  plan:

  - in_parallel:
      # Get the following resources

    - get: source-code
      trigger: true

    - get: application-version 

  # There's lots of things that can go wrong in this job that won't cause the job to fail. 
  # (If both tasks fail, for any reason at all, the job will think it's just a patch change!)
  # This task runs all those things and fails if they go wrong, making sure we don't miss any unexpected errors.
  - task: test-executions
    file: source-code/pipeline/tasks/test-detect-change-executions.tpl
    params:
      PREV: {{ $prev_swagger_file }}
      NEW: {{ $new_swagger_file }}

  # If a breaking change is detected, task fails (without breaking pipeline), and application number receives a patch bump
  - try:
      task: if-breaking-change-then-fail-dont-break-bump-major
      file: source-code/pipeline/tasks/breaking-change.tpl
      params:
        PREV: {{ $prev_swagger_file }}
        NEW: {{ $new_swagger_file }}
      on_failure:
        put: application-version 
        params: {bump: major}

      # If no breaking change found:
      on_success:
        # If a material (but non-breaking) change is detected, task succeeds, and application version number receives a minor bump
        # If no material change is detected, task fails (without breaking pipeline), and application version number receives a patch bump
        try:
          task: try-on-success-bump-minor-on-failure-bump-patch
          file: source-code/pipeline/tasks/minor-or-patch.tpl
          params:
            PREV: {{ $prev_swagger_file }}
            NEW: {{ $new_swagger_file }}
          on_success:
            put: application-version 
            params: {bump: minor}
          on_failure:
            put: application-version 
            params: {bump: patch}

merge:
- template: resources/git/source-code.tpl
- template: resources/semver/application-version.tpl