# If you make any changes to this file, ensure you make any equivalent changes to detect-change-allow-breaking-changes.tpl

{{ $group := .group }}
{{ $prev_swagger_file := .prev_swagger_file }}
{{ $new_swagger_file := .new_swagger_file }}

groups:
- name: {{ $group }}
  jobs:
    - "detect-change"

jobs:
- name: "detect-change"
  serial: true
  plan:

  - in_parallel:
      # Get the following resources

    - get: source-code
      trigger: false

    - get: application-version 

  # There's lots of things that can go wrong in this job that won't cause the job to fail. 
  # (If both tasks fail, for any reason at all, the job will think it's just a patch change!)
  # This task runs all those things and fails if they go wrong, making sure we don't miss any unexpected errors.
  - task: test-executions
    file: source-code/pipeline/tasks/test-detect-change-executions.tpl
    params:
      PREV: {{ $prev_swagger_file }}
      NEW: {{ $new_swagger_file }}

  # If a breaking change is detected, task fails, breaking the pipeline
  - task: if-breaking-change-then-fail
    file: source-code/pipeline/tasks/breaking-change.tpl
    params:
      PREV: {{ $prev_swagger_file }}
      NEW: {{ $new_swagger_file }}
    on_success:
      # If a material (but non-breaking) change is detected, task succeeds, and application version number receives a minor bump
      # If no material change is detected, task fails (without breaking pipeline), and application version number receives a patch bump
      try:
        task: try-on-success-bump-minor-on-failure-bump-patch
        file: source-code/pipeline/tasks/minor-or-patch.tpl
        params:
          PREV: {{ $prev_swagger_file }}
          NEW: {{ $new_swagger_file }}
        on_success:
          put: application-version 
          params: {bump: minor}
        on_failure:
          put: application-version 
          params: {bump: patch}

merge:
- template: resources/git/source-code.tpl
- template: resources/semver/application-version.tpl