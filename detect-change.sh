#!/bin/sh
# This script should be run with sh in an alpine environment.

BREAKING=$(oasdiff breaking $1 $2)
CHANGELOG=$(oasdiff changelog $1 $2)

SEMVER_PARTS=$(cat version.version | tr "." "\n")

MAJOR=$(echo $SEMVER_PARTS | cut -d' ' -f1)
MINOR=$(echo $SEMVER_PARTS | cut -d' ' -f2)
PATCH=$(echo $SEMVER_PARTS | cut -d' ' -f3)

CHANGE_TYPE=""

if [ -n "$BREAKING" ]
then 
    echo "Breaking changes found between old swagger file and new one. Exiting with error"
    exit 1

    # USE NOTE: Either include the above 2 lines (default) OR the below 3 lines (ONLY if you've decided to push a breaking change)

    #echo "Breaking changes found. Not exiting. Performing major bump on version number"
    #let "MAJOR=$((MAJOR+1))"
    #CHANGE_TYPE="Major"
elif [ -n "$CHANGELOG" ]
then 
    echo "Material change found"
    let "MINOR=$((MINOR+1))"
    CHANGE_TYPE="Minor"
else 
    echo "No material change found"
    let "PATCH=$((PATCH+1))"
    CHANGE_TYPE="Patch"
fi

echo ${MAJOR}.${MINOR}.${PATCH} > new-version-number.txt
cat new-version-number.txt

echo "$CHANGE_TYPE version bump" > commit-message.txt
cat commit-message.txt
