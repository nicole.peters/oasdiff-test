FROM alpine:latest

RUN apk update \
    && apk add curl --no-cache \
    && curl -OL https://github.com/Tufin/oasdiff/releases/download/v1.6.4/oasdiff_1.6.4_linux_amd64.apk \
    && apk add oasdiff_1.6.4_linux_amd64.apk --allow-untrusted \
    && apk add git bash --no-cache

COPY . /
RUN chmod 777 /