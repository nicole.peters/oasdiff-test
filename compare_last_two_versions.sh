# $1: relative path to version file
# This script must be run inside the git repository where the version file is located

mapfile -t last_two_commits < <(git log -n 2 --pretty=format:%H -- $1)
current_version=($(git show ${last_two_commits[0]}:$1 | tr "." " "))
prev_version=($(git show ${last_two_commits[1]}:$1 | tr "." " "))

# If the repository has a tag with the current version number, there has been a deployment w this version number (the major update we wanted) & we don't want to allow breaking changes again
# If there is no tag with the current version number, the version number was changed but doesn't have a deployment associated with it yet, meaning it was bumped manually to prepare for a major update
tagged_with_current_version=$(git tag -l $(git show ${last_two_commits[0]}:$1))

if [ ${current_version[0]} = $(expr ${prev_version[0]} + 1) ] && [ -e $tagged_with_current_version ]; then
    echo 1
else 
    echo 0
fi
