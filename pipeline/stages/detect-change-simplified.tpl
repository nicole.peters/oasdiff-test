{{ $group := .group }}
{{ $prev_swagger_file := .prev_swagger_file }}
{{ $new_swagger_file := .new_swagger_file }}

groups:
- name: {{ $group }}
  jobs:
    - "detect-change"

jobs:
- name: "detect-change"
  serial: true
  plan:

  - in_parallel:
      # Get the following resources

    - get: source-code
      trigger: false

    - get: application-version 

  - put: oasdiff-image
    params:
      build: source-code
      tag_as_latest: true

  - task: detect-change
    config:
      platform: linux 
      image_resource:
        type: docker-image
        source:
          repository: harbor.finbourne.com/non-harbor-images/docker.io/finbourne/oasdiff
          username: robot$concourse
          password: ((harbor.non_harbor_images_token))

      inputs:
        - name: source-code 
        - name: application-version

      outputs:
        - name: version-output

      run:
        path: /bin/bash 
        args:
        - -ce 
        - |
          version_file=source-code/new-version-number.txt
          cat source-code/version.version > $version_file

          cwd=$(pwd)
          echo $cwd
          cd source-code
          pwd
          cat version.version
          breaking_changes_allowed=$(bash compare_last_two_versions.sh version.version)
          echo "Breaking changes allowed:"
          echo $breaking_changes_allowed
          cd $cwd
          pwd

          ls -l

          # Script sets new version number and sets commit api semaphore
          # Job will fail if breaking changes are detected, unless they are allowed
          bash source-code/get_sdk_version.sh source-code/{{ $prev_swagger_file }} source-code/{{ $new_swagger_file }} $version_file semaphore $breaking_changes_allowed
          export version=$(cat $version_file)
          mv $version_file version-output/new-version-number.txt
          echo "Bump to $version" > version-output/commit-message.txt
          cat version-output/commit-message.txt



        #   cd source-code
        #   chmod 777 detect-change.sh 
        #   ./detect-change.sh {{ $prev_swagger_file }} {{ $new_swagger_file }}
        #   cd ..
        #   mv source-code/new-version-number.txt version-output/new-version-number.txt
        #   mv source-code/commit-message.txt version-output/commit-message.txt

  - task: commit-new-version-number
    config:
      platform: linux
      image_resource:
        type: docker-image
        source:
          repository: harbor.finbourne.com/non-harbor-images/docker.io/finbourne/oasdiff
          username: robot$concourse
          password: ((harbor.non_harbor_images_token))

      inputs:
        - name: version-output
        - name: source-code

      outputs:
        - name: source-code

      run:
        path: sh
        args:
        - -exc
        - | 
          cd source-code
          #git clone https://gitlab.com/nicole.peters/oasdiff-test.git
          cp ../version-output/new-version-number.txt version.version
          git add version.version
          git config user.email "git@localhost"
          git config user.name "git"
          git commit --allow-empty -m "$(cat ../version-output/commit-message.txt)"
          git tag $(cat version.version)

  - put: source-code
    params: {repository: source-code}


merge:
- template: resources/git/source-code.tpl
- template: resources/semver/application-version.tpl
- template: resources/docker/oasdiff-image.tpl