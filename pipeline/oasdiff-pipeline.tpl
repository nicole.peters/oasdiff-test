merge:

  - template: stages/detect-change-simplified.tpl
    args:
      group: main
      # file paths, from root directory of source code
      prev_swagger_file: swagger.json 
      new_swagger_file: swaggercopy.json 
