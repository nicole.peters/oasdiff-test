resources:
- name: application-version
  type: semver
  source:
    driver: git
    branch: main
    uri: git@gitlab.com:nicole.peters/oasdiff-test.git
    file: version.version
    initial_version: 0.1.0
    private_key: ((gitlab.id_rsa))