resources:
- name: source-code
  type: git
  source:
    uri: git@gitlab.com:nicole.peters/oasdiff-test.git
    branch: main
    private_key: ((gitlab.id_rsa))