resources:
- name: oasdiff-image
  type: docker-image
  source:
    repository: harbor.finbourne.com/non-harbor-images/docker.io/finbourne/oasdiff
    username: robot$concourse
    password: ((harbor.non_harbor_images_token))