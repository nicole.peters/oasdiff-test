CONCOURSE_TEAM=client-tech
CONCOURSE_PIPELINE_NAME=nicole.oasdiff-test

uav merge -p oasdiff-pipeline.tpl -o .pipeline.yaml
fly login -n $CONCOURSE_TEAM -t $CONCOURSE_TEAM -c https://concourse.finbourne.com
fly -t $CONCOURSE_TEAM set-pipeline --pipeline $CONCOURSE_PIPELINE_NAME --config .pipeline.yaml