#!/bin/bash

# $1: old swagger file, $2: new swagger file, $3: version number file, $4: commit api semaphore file, $5: "1" if breaking changes are allowed, else "0"
echo $1
echo $2
echo $3
echo $4
echo $5
echo $6 # jic

cat $3


breaking=$(oasdiff breaking $1 $2)
changelog=$(oasdiff changelog $1 $2)

if [ -n "$changelog" ] ; then
    touch $4 
    echo "Material changes found, setting commit api semaphor"

    # determine new version number
    version_array=($(cat $3 | tr "." " "))
    if [ -n "$breaking" ] ; then
        echo "Breaking changes found"
        oasdiff breaking $1 $2

        # We want to let breaking changes through IF the most recent change to the version number was a major bump
        # (this would mean that someone chose to let the breaking changes through by manually major-bumping the version number)
        if [ $5 = 1 ] ; then 
            echo "Allowing breaking changes to go through as a major version bump"
            echo ${version_array[0]}.0.0 > $3 # since the major number was just bumped, leave it as is
        else 
            echo "Blocking breaking changes" 
            echo "Please discuss these changes with @duck-ctech. It may be necessary to save these changes for a later time, to be released alongside other breaking changes in a major version update. You may also want to reconsider deploying these changes at all."
            echo "When it is time for your breaking changes to go through, you will need to run the bump-sdk-major job and then run this job again." 
            exit 1
        fi 
    else
        echo ${version_array[0]}.$((${version_array[1]}+1)).0 > $3 # minor bump
    fi
else
    echo "No material changes found, setting commit api semaphor"
fi

cat $3