# oasdiff test

## Description

Example pipeline that uses a script to compare two API definition files, determine whether the changes between them are breaking (major change), material but nonbreaking (minor change), or nonmaterial (patch change), and bump the version number as applicable, pushing this change using git.

Default behavior breaks pipeline on breaking change, bumps minor version number on material but nonbreaking change, and bumps patch version number on nonmaterial change. Alternative behavior bumps major version number on breaking change instead of breaking pipeline.

The script that makes up the majority of this project uses the open source tool [oasdiff](https://github.com/Tufin/oasdiff). An (unfortunately incomplete) list of changes oasdiff detects can be found [here](https://github.com/Tufin/oasdiff/blob/main/BREAKING-CHANGES-EXAMPLES.md). Error and warning level changes will be returned by `oasdiff breaking x y`, and error, warning, and info level changes will be returned by `oasdiff changelog x y`. Neither command will return nonmaterial changes, such as changes to descriptions or examples. 

## Use

Set up pipeline: `cd pipeline; ./compile-and-fly.sh`

Choose which two files will be compared by editing `pipeline/oasdiff-pipeline.tpl`

Example files to choose from:
- `swagger.json`: default
- `swaggercopy.json`: file with description and example differences from `swagger.json` but no material differences
- `swaggerdiff.json`: file with several nonmaterial differences from `swagger.json` but only one material one
- `swaggerdiff_april.json`: file with several major differences (breaking changes) from the others

To switch between ways of dealing with breaking changes, go to `detect-change.sh` and comment out the appropriate line and uncomment the appropriate lines (as explained there). 