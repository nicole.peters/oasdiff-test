#!/bin/sh

if [ -n "$(oasdiff breaking $1 $2 -f text)" ]
then
    echo "Breaking changes found between old swagger file and new one. Exiting with error"
    exit 1
#elif [ -n "$(oasdiff changelog $1 $2 | grep "endpoint added")" ]
elif [ -n "$(oasdiff changelog $1 $2)" ]
then
    echo "minor" > change.txt
#elif [ -n "$(oasdiff changelog $1 $2)" ]
#then 
#    echo "patch" >> change.txt
else
#    echo "none" >> change.txt
    echo "patch" > change.txt
fi