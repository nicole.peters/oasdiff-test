


{{ $group := .group }}

groups:
- name: {{ $group }}
  jobs:
    - "start"

jobs:
- name: "start"
  serial: true
  plan:

  - in_parallel:
      # Get the following resources

    - get: source-code
      trigger: true

    - get: application-version 

  #- put: docker-image 
  #  params:
  #    build: source-code/.
  #    tag_as_latest: true

  # if changelog is empty, bump patch; else, bump minor
  - try:
      task: try-on-success-bump-minor-on-failure-bump-patch 
      config:
        platform: linux
        image_resource:
          type: docker-image 
          source: {repository: nicolepeters/oasdiff-test}

        inputs:
          - name: source-code

        run:
          path: sh
          args: 
          - -exc
          - |
            test -n "$(oasdiff changelog source-code/swagger.json source-code/swagger.json)" && echo "minor change" || (echo "patch change" && exit 1)
      on_success:
        put: application-version
        params: {bump: minor}
      on_failure: 
        put: application-version 
        params: {bump: patch}
        
        #on_success:
        #  run:
        #    path: echo
        #    args: [woohoo]




merge:
- template: resources/git/source-code.tpl
- template: resources/semver/application-version.tpl
#- template: resources/docker/docker-image.tpl